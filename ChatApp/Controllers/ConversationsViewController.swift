//
//  ViewController.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import UIKit
import FirebaseAuth
class ConversationsViewController: UIViewController {

    let chatImage = ImageView(imageName: "chatdesign")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        chatImage.contentMode = .scaleToFill
        setupViews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        validateAuth()
    }
    private func validateAuth(){
        if FirebaseAuth.Auth.auth().currentUser == nil  {
            let vc = LoginViewController()
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .fullScreen
            present(nav, animated: true)
        }
    }
    func setupViews(){
        view.addSubview(chatImage)
        
        NSLayoutConstraint.activate([
        
            chatImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 80.heightRatio),
            chatImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -4),
            chatImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            chatImage.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70.heightRatio)
            
        ])
    }
}
