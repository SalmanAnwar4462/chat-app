//
//  LoginViewController.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    let logo = ImageView(imageName: "logo")
    let emailField = TextField(placeHolder: "Email Address....", secureTextEntry: false)
    let passwordField = TextField(placeHolder: "Password....", secureTextEntry: true)
    let loginButton = Button(backgroundColor: .link, radius: 20.heightRatio, title: "Log In", font: .systemFont(ofSize: 25), titleColor: .white)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        emailField.becomeFirstResponder()
        emailField.setLeftPaddingPoints(10)
        passwordField.setLeftPaddingPoints(10)
        emailField.returnKeyType = .continue
        passwordField.returnKeyType = .done
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register", style: .done, target: self, action: #selector(didTapRegister))
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        emailField.delegate = self
        passwordField.delegate = self
        setUpViews()
    }
    @objc private func didTapRegister(){
        let vc = RegisterViewController()
        vc.title = "Register Account"
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc private func loginButtonTapped(){
       
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        guard let email = emailField.text, let password = passwordField.text, !email.isEmpty, !password.isEmpty, password.count >= 6 else {
            alertUserLogin()
            return
        }
        FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password, completion: {[weak self] authResult, error in
            guard let strongSelf = self else {
                return
            }
            guard let result = authResult, error == nil else{
                print("Error Logging In")
                return
            }
            let user = result.user
            print("Logged In User: \(user)")
            strongSelf.navigationController?.dismiss(animated: true, completion: nil)
        })
    }
    
    func alertUserLogin(){
        let alert = UIAlertController(title: "Whoops", message: "Please enter all information for login", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    func setUpViews(){
        view.addSubview(logo)
        view.addSubview(emailField)
        view.addSubview(passwordField)
        view.addSubview(loginButton)
     
        NSLayoutConstraint.activate([
        
            logo.topAnchor.constraint(equalTo: view.topAnchor, constant: 70.heightRatio),
            logo.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 60.widthRatio),
            logo.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -60.widthRatio),
            logo.heightAnchor.constraint(equalToConstant: 200.heightRatio),
            
            emailField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            emailField.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 6.heightRatio),
            emailField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30.widthRatio),
            emailField.heightAnchor.constraint(equalToConstant: 70.heightRatio),
            
            passwordField.leadingAnchor.constraint(equalTo: emailField.leadingAnchor),
            passwordField.topAnchor.constraint(equalTo: emailField.bottomAnchor, constant: 20.heightRatio),
            passwordField.trailingAnchor.constraint(equalTo: emailField.trailingAnchor),
            passwordField.heightAnchor.constraint(equalToConstant: 70.heightRatio),
            
            loginButton.leadingAnchor.constraint(equalTo: emailField.leadingAnchor),
            loginButton.topAnchor.constraint(equalTo: passwordField.bottomAnchor, constant: 70.heightRatio),
            loginButton.trailingAnchor.constraint(equalTo: emailField.trailingAnchor),
            loginButton.heightAnchor.constraint(equalToConstant: 70.heightRatio),
        ])
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        }
        else if textField == passwordField {
            loginButtonTapped()
        }
        return true
    }
}
