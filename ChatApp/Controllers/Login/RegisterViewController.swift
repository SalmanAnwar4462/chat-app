//
//  RegisterViewController.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import UIKit
import FirebaseAuth

class RegisterViewController: UIViewController {
    
    let logo = ImageView(imageName: "logo1", cornerRadius: 80.heightRatio)
    let firstField = TextField(placeHolder: "First Name....", secureTextEntry: false)
    let lastField = TextField(placeHolder: "Last Name....", secureTextEntry: false)
    let emailField = TextField(placeHolder: "Email Address....", secureTextEntry: false)
    let passwordField = TextField(placeHolder: "Password....", secureTextEntry: true)
    let loginButton = Button(backgroundColor: .systemGreen, radius: 20.heightRatio, title: "Register", font: .systemFont(ofSize: 25), titleColor: .white)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        firstField.becomeFirstResponder()
        firstField.setLeftPaddingPoints(10)
        lastField.setLeftPaddingPoints(10)
        emailField.setLeftPaddingPoints(10)
        passwordField.setLeftPaddingPoints(10)
        firstField.returnKeyType = .continue
        lastField.returnKeyType = .continue
        emailField.returnKeyType = .continue
        passwordField.returnKeyType = .done
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "LogIn", style: .done, target: self, action: #selector(didTapRegister))
        loginButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)
        emailField.delegate = self
        passwordField.delegate = self
        logo.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapChangeProfilePic))
        logo.addGestureRecognizer(gesture)
        logo.layer.masksToBounds = true
        logo.layer.borderWidth = 2
        logo.layer.borderColor = UIColor.black.cgColor
        setUpViews()
    }
    @objc private func didTapRegister(){
        let vc = LoginViewController()
        vc.title = "Register Account"
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc private func didTapChangeProfilePic(){
        presentPhotoActionSheet()
    }
    @objc private func registerButtonTapped(){
        firstField.resignFirstResponder()
        lastField.resignFirstResponder()
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        guard let firstName = firstField.text,
              let lastName = lastField.text,
              let email = emailField.text,
              let password = passwordField.text,
              !firstName.isEmpty,
              !lastName.isEmpty,
              !email.isEmpty,
              !password.isEmpty,
              password.count >= 6 else {
            alertUserLogin()
            return
        }
        DatabaseManager.shared.userExist(email: email, completion: { [weak self] exists in
            guard let strongSelf = self else {
                return
            }
            guard !exists else{
                //user already exists
                strongSelf.alertUserLogin(message: "Looks like a user account for that email address already exists.")
                return
        }
            FirebaseAuth.Auth.auth().createUser(withEmail: email, password: password, completion: { authResult, error in
                guard authResult != nil, error == nil else{
                    print("Error Creating User")
                    return
                }
                DatabaseManager.shared.insertUser(user: ChatAppUser(firstName: firstName, lastName: lastName, emailAddress: email))
                strongSelf.navigationController?.dismiss(animated: true, completion: nil)
            })
            })
        }
    func alertUserLogin(message: String = "Please enter all information to create a new account"){
        let alert = UIAlertController(title: "Whoops", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    func setUpViews(){
        view.addSubview(logo)
        view.addSubview(firstField)
        view.addSubview(lastField)
        view.addSubview(emailField)
        view.addSubview(passwordField)
        view.addSubview(loginButton)
        
        NSLayoutConstraint.activate([
            
            logo.topAnchor.constraint(equalTo: view.topAnchor, constant: 120.heightRatio),
            logo.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logo.widthAnchor.constraint(equalToConstant: 160.widthRatio),
            logo.heightAnchor.constraint(equalToConstant: 160.heightRatio),
            
            firstField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            firstField.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 30.heightRatio),
            firstField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30.widthRatio),
            firstField.heightAnchor.constraint(equalToConstant: 70.heightRatio),
            
            lastField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            lastField.topAnchor.constraint(equalTo: firstField.bottomAnchor, constant: 20.heightRatio),
            lastField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30.widthRatio),
            lastField.heightAnchor.constraint(equalToConstant: 70.heightRatio),
            
            emailField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            emailField.topAnchor.constraint(equalTo: lastField.bottomAnchor, constant: 20.heightRatio),
            emailField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30.widthRatio),
            emailField.heightAnchor.constraint(equalToConstant: 70.heightRatio),
            
            passwordField.leadingAnchor.constraint(equalTo: emailField.leadingAnchor),
            passwordField.topAnchor.constraint(equalTo: emailField.bottomAnchor, constant: 20.heightRatio),
            passwordField.trailingAnchor.constraint(equalTo: emailField.trailingAnchor),
            passwordField.heightAnchor.constraint(equalToConstant: 70.heightRatio),
            
            loginButton.leadingAnchor.constraint(equalTo: emailField.leadingAnchor),
            loginButton.topAnchor.constraint(equalTo: passwordField.bottomAnchor, constant: 50.heightRatio),
            loginButton.trailingAnchor.constraint(equalTo: emailField.trailingAnchor),
            loginButton.heightAnchor.constraint(equalToConstant: 70.heightRatio),
        ])
    }
}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        }
        else if textField == passwordField {
            registerButtonTapped()
        }
        return true
    }
}
extension RegisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentPhotoActionSheet(){
        let actionSheet = UIAlertController(title: "Profile Picture", message: "How would you like to select a picture", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {[weak self] _ in
            self?.presentCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: {[weak self] _ in
            self?.presentPhotoPicker()
        }))
        present(actionSheet, animated: true)
    }
    
    func presentCamera(){
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    func presentPhotoPicker(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else{
            return
        }
        self.logo.image = selectedImage
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
