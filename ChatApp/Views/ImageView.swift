//
//  ImageView.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import Foundation
import UIKit

class ImageView: UIImageView {
    
    init(imageName: String = "", cornerRadius: CGFloat = 0, backgroundColor: UIColor = .clear) {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = cornerRadius
        self.contentMode = .scaleAspectFit
        image = UIImage(named: imageName)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
