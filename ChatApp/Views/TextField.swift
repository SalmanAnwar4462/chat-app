//
//  TextField.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import Foundation
import UIKit

class TextField: UITextField{
    init(placeHolder: String = "", secureTextEntry: Bool = false){
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.placeholder = placeHolder
        self.layer.cornerRadius = 20.heightRatio
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.black.cgColor
        self.isSecureTextEntry = secureTextEntry
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
