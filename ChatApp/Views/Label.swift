//
//  Label.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import Foundation
import UIKit

class Label: UILabel {
    init(text: String = "", font: UIFont, textColor: UIColor = .concrete(), numberOfLines:Int = 0){
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.text = text
        self.font = font
        self.textColor = textColor
        self.numberOfLines = numberOfLines
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
