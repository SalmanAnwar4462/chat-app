//
//  Button.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import Foundation
import UIKit

class Button: UIButton{
    init(backgroundColor: UIColor = .concrete(), radius: CGFloat = 0.heightRatio, title: String = "", font: UIFont = .systemFont(ofSize: 12), titleColor: UIColor = .clear){
        super.init(frame: .zero)
        self.backgroundColor = backgroundColor
//        self.setImage(UIImage(named: imageName), for: .normal)
        self.layer.cornerRadius = radius
        self.setTitle(title, for: .normal)
        self.setTitleColor(titleColor, for: .normal)
        self.titleLabel?.font = font
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
