//
//  UIViews.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import Foundation
import UIKit
extension Int{
    
    var widthRatio:CGFloat {
        let width = UIScreen.main.bounds.width/414
        return CGFloat(self)*width
    }
    var heightRatio: CGFloat {
        let height = UIScreen.main.bounds.height/896
        return CGFloat(self)*height
    }
}
extension UIView{
    func setGradientBackground(colors: [UIColor], cornerRadius: CGFloat = 0) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.name = "gradient"
        gradientLayer.frame = bounds
        gradientLayer.cornerRadius = cornerRadius
        gradientLayer.colors = colors.map {$0.cgColor}
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0,y: 0.2)
        gradientLayer.endPoint = CGPoint(x: 0.4,y: 1.0)
        layer.insertSublayer(gradientLayer, at: 0)
    }
}
extension CALayer {
    func applyShadow(opacity: Float = 0.3){
        let colorShadow = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 1.0)
        shadowColor = colorShadow.cgColor
        shadowOpacity = opacity
        shadowOffset = CGSize(width: 0, height: 12)
        shadowRadius = 8
        shadowPath = nil
    }
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UIViewController{
    func loader() -> UIAlertController{
        let alert = UIAlertController(title: nil, message: "Please Wait.....", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 5, y: 5, width: 80, height: 60))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.large
        loadingIndicator.startAnimating()
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true)
            return alert
        }
        func stoploader(loader: UIAlertController){
            DispatchQueue.main.async {
                loader.dismiss(animated: true, completion: nil)
            }
    }
}
