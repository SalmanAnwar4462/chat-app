//
//  ColorExtension.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import Foundation
import UIKit

public extension UIColor{
    static func concrete() -> UIColor {
        return .init(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
    }
    static func woodsmoke() -> UIColor {
        return .init(red: 15/255, green: 16/255, blue: 17/255, alpha: 1.0)
    }
    static func skyblue() -> UIColor {
        return .init(red: 116/255, green: 162/255, blue: 255/255, alpha: 1.0)
    }
    static func nevada() -> UIColor {
        return .init(red: 106/255, green: 108/255, blue: 113/255, alpha: 1.0)
    }
    static func heliotrope() -> UIColor {
        return .init(red: 235/255, green: 77/255, blue: 255/255, alpha: 1.0)
    }
    static func white() -> UIColor {
        return .init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
    }
    static func osloGrey() -> UIColor {
        return .init(red: 131/255, green: 134/255, blue: 139/255, alpha: 1.0)
    }
    static func alabaster() -> UIColor {
        return .init(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
    }
    static func mischka() -> UIColor {
        return .init(red: 230/255, green: 231/255, blue: 233/255, alpha: 1.0)
    }
    static func shuttlegrey() -> UIColor {
        return .init(red: 81/255, green: 83/255, blue: 87/255, alpha: 1.0)
    }
    static func Tuna() -> UIColor {
        return .init(red: 48/255, green: 49/255, blue: 52/255, alpha: 1.0)
    }
    static func translucentBlack() -> UIColor {
        return .init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.4)
    }
    static func whiteonDark() -> UIColor {
            return .init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
    }
    static func carnationPink() -> UIColor {
            return .init(red: 255/255, green: 126/255, blue: 176/255, alpha: 1.0)
    }
    static func lightRoyalBlue() -> UIColor {
            return .init(red: 48/255, green: 77/255, blue: 255/255, alpha: 1.0)
    }
    static func Lochmara() -> UIColor {
        return .init(red: 0/255, green: 125/255, blue: 197/255, alpha: 1.0)
    }
    static func SantasGrey() -> UIColor {
            return .init(red: 157/255, green: 159/255, blue: 164/255, alpha: 1.0)
    }
    static func Malibu() -> UIColor {
        return .init(red: 78/255, green: 190/255, blue: 255/255, alpha: 1.0)
    }
    static func Abbey() -> UIColor {
        return .init(red: 65/255, green: 66/255, blue: 69/255, alpha: 1.0)
    }
    static func TertiaryO3() -> UIColor {
        return .init(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
    }
    static func brightBlue() -> UIColor {
        return .init(red: 0/255, green: 111/255, blue: 255/255, alpha: 1.0)
    }
    static func greenishTurquoise() -> UIColor {
        return .init(red: 0/255, green: 255/255, blue: 172/255, alpha: 1.0)
    }
    static func sunYellow() -> UIColor {
        return .init(red: 255/255, green: 214/255, blue: 34/255, alpha: 1.0)
    }
    static func brightLilac() -> UIColor {
        return .init(red: 206/255, green: 81/255, blue: 255/255, alpha: 1.0)
    }
    static func Black10() -> UIColor {
        return .init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1)
    }
    static func Black20() -> UIColor {
        return .init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2)
    }
    static func mountainMeadow() -> UIColor {
        return .init(red: 22/255, green: 219/255, blue: 147/255, alpha: 1.0)
    }
    static func peach() -> UIColor {
        return .init(red: 255/255, green: 175/255, blue: 131/255, alpha: 1.0)
    }
    static func bubblegum() -> UIColor {
        return .init(red: 255/255, green: 117/255, blue: 185/255, alpha: 1.0)
    }
    static func catskillWhite() -> UIColor {
        return .init(red: 229/255, green: 233/255, blue: 242/255, alpha: 1.0)
    }
    static func mango() -> UIColor {
        return .init(red: 255/255, green: 160/255, blue: 34/255, alpha: 1.0)
    }
    static func lemon() -> UIColor {
        return .init(red: 232/255, green: 255/255, blue: 81/255, alpha: 1.0)
    }
}
