//
//  AppDelegate.swift
//  ChatApp
//
//  Created by Salman Anwar on 06/06/2022.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navController: UINavigationController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure() 
        presentViewController()
        return true
    }
    func presentViewController() {
        window = UIWindow()
        window?.makeKeyAndVisible()
        navController = UINavigationController(rootViewController: ConversationsViewController())
        window?.rootViewController = navController
    }
}

